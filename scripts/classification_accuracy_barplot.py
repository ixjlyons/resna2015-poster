#!/usr/bin/env python

import settings
try:
    import cPickle as pickle
except ImportError:
    import pickle
from numpy import arange, savetxt
import matplotlib as mpl
settings.mplrc['axes.facecolor'] = 'none'
mpl.rcParams.update(settings.mplrc)
import matplotlib.pyplot as plt

data = pickle.load(open(settings.datafile, 'rb'))

pid_list = []
items = sorted(data.items())
#pid_list = [item[0] for item in items]
pid_list = [str(i+1) for i in range(len(items))]
acc_arm = [item[1][0].get_avg_accuracy() for item in items]
acc_leg = [item[1][1].get_avg_accuracy() for item in items]

# bar properties
ind = arange(1, len(pid_list)+1)
width = 0.4
margin = 0.01
#colors = ['#598aad', '#83b263']
colors = ['#b15050', '#3d77a6']
alphas = [0xdc, 0xdc]
ylim = (0.8, 1)

fig = plt.figure()
ax = plt.gca()

rects_arm = ax.bar(ind-margin/2-width/2, acc_arm, width,
                   align='center',
                   alpha=alphas[0]/256.0,
                   color=colors[0],
                   linewidth=0,
                   label='arm')

for i in range(len(acc_arm)):
    label = "%.2f" % round(acc_arm[i], 2)
    ax.text(ind[i]-margin/2-width/2, ylim[0]+(ylim[1]-ylim[0])/10.0, label,
            color=settings.poster_bg, va='top', ha='center')

rects_leg = ax.bar(ind+margin/2+width/2, acc_leg, width,
                   align='center',
                   alpha=alphas[1]/256.0,
                   color=colors[1],
                   linewidth=0,
                   label='leg')

for i in range(len(acc_arm)):
    label = "%.2f" % round(acc_leg[i], 2)
    ax.text(ind[i]+margin/2+width/2, ylim[0]+(ylim[1]-ylim[0])/10.0, label,
            color=settings.poster_bg, va='top', ha='center')

# grid
ax.grid(axis='y', color=settings.poster_bg, linestyle='-', alpha=0.4)

# legend
legend = ax.legend(frameon=True, loc='upper right', framealpha=0.8)
rect = legend.get_frame()
rect.set_facecolor('#efefef')
rect.set_linewidth(0.0)

# remove tick marks on both axes
ax.xaxis.set_ticks_position('none')
ax.yaxis.set_ticks_position('none')
# adust spines
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['left'].set_linewidth(0.5)
ax.spines['bottom'].set_linewidth(0.5)

ax.set_ylabel('average accuracy')
ax.set_xlabel('participant')


ax.set_ylim(ylim)
#ax.set_xlim(
#    ind[0]-margin/2.-width-margin-width-0.2, 
#    ind[-1]+margin/2.+width+margin+width+0.2)
ax.set_xticks(ind)
ax.set_xticklabels(pid_list)


fig.set_size_inches(9, 7)
fig.subplots_adjust(left=0.15, bottom=0.15)

if settings.saveplots:
    fig.savefig(
        settings.savepath+'classification_accuracy_barplot.'+settings.saveext,
        dpi=300,
        bbox_inches='tight')

if settings.showplots:
    plt.show()
