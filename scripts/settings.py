datafile = 'data.pkl'
savepath = "../fig/"
saveext = 'pdf'
showplots = False
saveplots = True 

poster_bg = '#fafafa'

white = poster_bg
black = '#4a4a4a'

mplrc = {
    'font.family': 'sans-serif',
    'font.serif': ['Tenor Sans'],
    'font.sans-serif': ['Tenor Sans'],
    'font.size': 22,
    'text.color': black,
    'axes.labelsize': 28,
    'legend.fontsize': 22,
    'xtick.labelsize': 22,
    'ytick.labelsize': 22,
    'axes.labelcolor': black,
    'axes.edgecolor': black,
    'xtick.color': black,
    'ytick.color': black,
    'savefig.facecolor': 'none'
}
