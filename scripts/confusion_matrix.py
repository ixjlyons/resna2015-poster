import settings
try:
    import cPickle as pickle
except ImportError:
    import pickle
import numpy as np
from pygesture import classification
import matplotlib as mpl
settings.mplrc['ytick.major.pad'] = 12
mpl.rcParams.update(settings.mplrc)
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from matplotlib.patches import Rectangle

fontsizes = {'diag_accuracy': '20', 'offdiag_accuracy': '18', 'diag_std': '16'}

def generate_plot(cm, acc_std, fontsizes, labels=None):
    if labels is None:
        labels = cm.labels

    data = cm.data_norm
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.matshow(data, cmap='hot_r')

    # add numerical accuracies along the diagonal
    for idx, lbl in enumerate(cm.labels):
        acc = round(data[idx, idx], 2)
        std = round(acc_std[idx], 2)
        if acc > 0.5:
            color = settings.white
        else:
            color = settings.black
        ax.text(idx, idx-0.2, str(acc),
            color=color, va='center', ha='center',
            size=fontsizes['diag_accuracy'])
        ax.text(idx, idx+0.2, r'$\pm$' + str(std),
            color=color, va='center', ha='center',
            size=fontsizes['diag_std'])

    # add numerical accuracies for off-diagonals
    notes = np.where(data >= 0.01)
    for i in range(np.size(notes[0])):
        x, y = notes[0][i], notes[1][i]
        if x != y:
            num = round(data[x, y], 2)
            if num > 0.5:
                color = settings.white
            else:
                color = settings.black
            ax.text(y, x, str(num),
                color=color, va='center', ha='center',
                size=fontsizes['offdiag_accuracy'])

    # set tick labels to the gesture abbreviations
    majorLocator = MultipleLocator(1)
    ax.xaxis.set_major_locator(majorLocator)
    ax.yaxis.set_major_locator(majorLocator)
    ax.set_xticklabels(['']+labels)
    ax.set_yticklabels(['']+labels)
    ax.set_xlabel('predicted gesture')
    ax.set_ylabel('performed gesture')
    ax.xaxis.set_label_position('top')
    ax.xaxis.labelpad = 12

    # thin out the spines
    for spine in ['top', 'right', 'bottom', 'left']:
        ax.spines[spine].set_linewidth(0.5)

    # remove tick marks
    ax.xaxis.set_ticks_position('none')
    ax.yaxis.set_ticks_position('none')

    return (fig, ax)

def get_accuracies(cm_list):
    cm_avg = classification.average_confusion_matrix(cm_list)
    acc_std = classification.accuracy_std(cm_list)
    return (cm_avg, acc_std)

# generate plots for all configurations without gestures l5 and l7
data = pickle.load(open('data.pkl', 'rb'))
items = sorted(data.items())

(cm_avg, acc_std) = get_accuracies([item[1][1] for item in items])

print(cm_avg.name + " average accuracy: " + str(cm_avg.get_avg_accuracy()))
(fig, ax) = generate_plot(cm_avg, acc_std, fontsizes, labels=items[0][1][0].labels)

fig.set_size_inches(8, 8)

if settings.saveplots:
    fig.savefig(
        settings.savepath+'confusion_matrix.'+settings.saveext,
        dpi=300,
        bbox_inches='tight')

if settings.showplots:
    plt.show()
