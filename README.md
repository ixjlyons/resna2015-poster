# resna2015-poster

This is my poster for the 2015 conference of the Rehabilitation Engineering and
Assistive Technology Society of North America (RESNA).

The poster was created using the following software:

* [Inkscape][inkscape]: the program used for making the poster itself.
* [Blender][blender]: used to pose human models, add additional objects, and
  ultimately render the 3D figures.
* [MakeHuman][makehuman]: used to generate the human models for import into
  Blender.
* [GIMP][gimp]: handled some small tasks like cropping the Blender figures and
  removing background content from photos.

Two of the [Google Fonts][google-fonts] were used in the poster: "Tenor Sans"
and "Fanwood Text" (both are under OFL).

The results figures were generated with Python (see `scripts/`), but this is
not of use to anyone currently as it depends on software I've written that is
not openly available yet (I hope to change that soon).


[inkscape]: https://inkscape.org/en/
[blender]: http://www.blender.org/
[makehuman]: http://www.makehuman.org/
[gimp]: http://www.gimp.org/
[google-fonts]: https://github.com/google/fonts/
